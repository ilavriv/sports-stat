import os

from app.common.json import MongoJSONEncoder

if __name__ == "__main__":
    from app.bootstrap import make_app

    app = make_app()

    app.json_encoder = MongoJSONEncoder
    app.run(host=os.getenv('HOST'), port=os.getenv('PORT'))
