from http import HTTPStatus

from .common import hal
from .common.exceptions import ApiException


__all__ = ('setup_errors', )


def _register_error_handler(app, status):
    @app.errorhandler(status)
    def handler(err):
        return hal.serialize_error(err), status


def setup_errors(app):
    statuses = [
        HTTPStatus.NOT_FOUND.value,
        HTTPStatus.METHOD_NOT_ALLOWED.value,
        HTTPStatus.UNAUTHORIZED.value,
        HTTPStatus.BAD_REQUEST.value
    ]

    @app.errorhandler(ApiException)
    def api_exception_handler(err):
        return hal.serialize_error(err), 422

    for status in statuses:
        _register_error_handler(app, status)
