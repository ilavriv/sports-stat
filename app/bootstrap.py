import inject
from flask import Flask
from flask_hal import HAL
from flask_jwt import JWT

from .common.db import db
from .common.rq import rq
from .common.mail import mail
from .modules.auth.methods import (
    identity,
    authenticate
)

from .config import config

from .routes import setup_routes
from .errors import setup_errors

from .jobs.some_jobs import *


def make_app():
    app = Flask(__name__)
    app.config.from_object(config.Config)
    HAL(app)
    db.init_app(app)
    JWT(app, authenticate, identity)
    setup_errors(app)
    rq.init_app(app)
    mail.init_app(app)
    setup_routes(app)

    return app
