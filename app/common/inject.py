import inject as inj
from werkzeug.local import LocalProxy


def configuration(binder):
    pass


inj.configure(configuration)

inject = LocalProxy(lambda: inj)
