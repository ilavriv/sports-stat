
class ApiException(Exception):

    def __init__(self, context):
        Exception.__init__(self, 'Api exception')
        self.context = context


class DataValidationException(ApiException):
    pass
