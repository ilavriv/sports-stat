import json
from datetime import datetime
from bson import ObjectId, DBRef
from mongoengine.base import BaseDocument


class MongoJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, BaseDocument):
            return obj._data
        elif isinstance(obj, ObjectId):
            return str(obj)
        elif isinstance(obj, datetime):
            return obj.isoformat()
        elif isinstance(obj, DBRef):
            return obj.id
        else:
            return json.JSONEncoder.default(self, obj)
