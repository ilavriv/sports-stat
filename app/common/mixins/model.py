from datetime import datetime

from mongoengine import (
    DateTimeField,
)

__all__ = ('TimestampMixin', )


class TimestampMixin(object):
    created_at = DateTimeField(default=datetime.now)
    updated_at = DateTimeField()

    def save(self, *args, **kwargs):
        now = datetime.now()

        if not self.created_at:
            self.created_at = now
        self.updated_at = now

        return super().save(*args, **kwargs)
