from werkzeug.local import LocalProxy
from flask import current_app

__all__ = ('logger', )

logger = LocalProxy(lambda: current_app.logger)
