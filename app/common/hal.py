import json
from flask_hal import document

from .json import MongoJSONEncoder
from .exceptions import ApiException

__all__ = ('serialize', 'serialize_error')


class _ApiDocument(document.Document):

    def to_json(self):
        return json.dumps(self.to_dict(), cls=MongoJSONEncoder)


def serialize(obj):
    return _ApiDocument(data={
        'data': obj
    }).to_dict()


def serialize_error(error):
    if isinstance(error, ApiException):
        error_payload = error.context
    else:
        error_payload = [str(error)]

    return _ApiDocument(data={
        'errors': error_payload
    }).to_dict()
