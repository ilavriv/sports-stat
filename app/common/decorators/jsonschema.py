from functools import wraps

from flask import request
from jsonschema import Draft7Validator

from app.common.exceptions import DataValidationException


def validate_jsonschema(schema):
    validator = Draft7Validator(schema)

    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            errors = [
                error.message for error in validator.iter_errors(request.json)]

            print(errors)

            if errors:
                raise DataValidationException(errors)

            return fn(*args, **kwargs)

        return decorator
    return wrapper
