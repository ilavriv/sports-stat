from .models import (
    Sport
)


class SportsService(object):

    def find_all(self):
        return [s for s in Sport.objects]

    def find_by_id(self, id):
        return Sport.objects.get(id=id)

    def create_sport(self, **sportData):
        sport = Sport(**sportData)
        sport.save()

        return sport
