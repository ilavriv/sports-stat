from http import HTTPStatus

from flask import request, abort
from flask.views import MethodView
from flask_jwt import jwt_required
from mongoengine.errors import (
    DoesNotExist
)

from app.common.inject import inject
from app.common.logger import logger
from app.common import hal

from .service import SportsService


class SportsAPI(MethodView):
    sports_service = inject.instance(SportsService)

    @jwt_required()
    def get(self):
        sports = self.sports_service.find_all()

        return hal.serialize(sports)

    @jwt_required()
    def post(self):

        try:
            sport = self.sports_service.create_sport(request.json)

            return hal.serialize(sport)
        except Exception as e:
            logger.error(e)
            abort(HTTPStatus.BAD_REQUEST.value)


class SportDetailsApi(MethodView):
    sports_service = inject.instance(SportsService)

    def get(self, sport_id):
        try:
            sport = self.sports_service.find_by_id(sport_id)

            return hal.serialize(sport)
        except DoesNotExist as e:
            logger.error(e)
            abort(HTTPStatus.NOT_FOUND.value)
