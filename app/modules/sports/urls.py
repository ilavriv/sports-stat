from flask.blueprints import Blueprint

from .views import (
    SportsAPI,
    SportDetailsApi
)

sports = Blueprint('sports', __name__)

sports.add_url_rule('', view_func=SportsAPI.as_view('sports_api'))
sports.add_url_rule(
    '/<sport_id>', view_func=SportDetailsApi.as_view('sport_details_api'))
