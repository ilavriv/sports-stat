from mongoengine import (
    StringField,
    IntField,
    Document
)

from app.common.mixins.model import TimestampMixin


class Sport(Document, TimestampMixin):
    meta = {'collection': 'sports'}

    name = StringField(max_length=120)
    short_description = StringField(max_length=300)
    description = StringField()
    start_year = IntField(max_value=9000)
