from flask import Blueprint

from .views import (
    TeamsAPI,
    TeamDetailsAPI
)

teams = Blueprint('teams', __name__)

teams.add_url_rule('', view_func=TeamsAPI.as_view('teams_api'))
teams.add_url_rule(
    '/<team_id>', view_func=TeamDetailsAPI.as_view('team_details_api'))
