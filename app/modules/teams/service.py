from .models import (
    Team
)


class TeamsService(object):

    def find_all(self):
        return [t for t in Team.objects]

    def find_by_id(self, id):
        return Team.objects.get(id=id)

    def persist(self, **teamData):
        team = Team(**teamData)
        team.save()

        return team
