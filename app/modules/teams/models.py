from mongoengine import (
    StringField,
    URLField,
    IntField,
    ReferenceField
)

from app.common.db import db
from app.common.mixins.model import TimestampMixin


class Team(db.Document, TimestampMixin):
    meta = {'collection': 'teams'}

    name = StringField(max_length=150, required=True)
    sport_id = ReferenceField('Sport')
    city = StringField(max_length=120)
    country = StringField(max_length=120)
    logo_url = URLField(max_length=120)
    start_year = IntField(max_value=9999)
