from http import HTTPStatus

from flask import request, abort
from flask.views import MethodView
from flask_jwt import jwt_required
from mongoengine.errors import DoesNotExist

from app.common import hal
from app.common.inject import inject

from .service import TeamsService


class TeamsAPI(MethodView):
    teams_service = inject.instance(TeamsService)

    @jwt_required()
    def get(self):
        payload = self.teams_service.find_all()

        return hal.serialize(payload)

    @jwt_required()
    def post(self):
        try:
            team = self.teams_service.persist(request.json)

            return hal.serialize(team)
        except Exception:
            abort(HTTPStatus.BAD_REQUEST.value)


class TeamDetailsAPI(MethodView):
    teams_service = inject.attr(TeamsService)

    def get(self, team_id):
        try:
            payload = self.teams_service.find_by_id(team_id)

            return hal.serialize(payload)
        except DoesNotExist:
            abort(HTTPStatus.NOT_FOUND.value)
