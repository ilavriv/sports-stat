from flask.views import MethodView
from flask_jwt import jwt_required

from app.common import hal
from app.common.inject import inject

from .service import CompetitionsService


class CompetitionsAPI(MethodView):
    competitions_service = inject.instance(CompetitionsService)

    @jwt_required()
    def get(self):
        payload = self.competitions_service.find_all()

        return hal.serialize(payload)
