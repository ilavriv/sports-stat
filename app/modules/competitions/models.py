from mongoengine import (
    StringField,
    IntField,
    Document,
    ReferenceField
)

from app.common.mixins.model import TimestampMixin


class Competition(Document, TimestampMixin):
    meta = {'colection': 'competitions'}

    sport_id = ReferenceField('Sport')
    name = StringField(max_length=120)
    start_year = IntField(max_value=9999)
    logo_url = StringField(max_length=200)
