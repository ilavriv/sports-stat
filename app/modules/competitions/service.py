from .models import Competition

class CompetitionsService(object):

    def find_all(self):
        return [ c for c in Competition.objects ]

