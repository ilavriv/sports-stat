from flask import render_template, request
from app.common.mail import mail


def send_email_invite(user):
    """
    Sends email user invite
    """
    html = render_template('user_invite.html', **{
        'user': user,
        'url': f'{request.url_root}users/p/{user.confirmation.confirm_id}'
    })

    mail.send_email(user.email, subject="Welcome to sport stats", html=html)
