import uuid

from mongoengine import (
    Document,
    EmbeddedDocument,
    EmbeddedDocumentField,
    UUIDField,
    EmailField,
    StringField,
    BooleanField,
)

from app.common.mixins.model import TimestampMixin


class Confirmation(EmbeddedDocument):
    confirm_id = UUIDField(default=str(uuid.uuid4()))
    accepted = BooleanField(default=False)


class User(Document, TimestampMixin):
    meta = {'collection': 'users'}

    email = EmailField(sparse=True, unique=True, required=True)
    password = StringField(max_length=120, required=True)
    first_name = StringField(max_length=120)
    last_name = StringField(max_length=120)
    email_confirmed = BooleanField(default=False)
    confirmation = EmbeddedDocumentField(Confirmation)
