from http import HTTPStatus

from flask import request, abort
from flask.views import MethodView
from mongoengine.errors import NotUniqueError

from app.common import hal
from app.common.decorators.jsonschema import validate_jsonschema
from app.common.inject import inject

from .service import UsersService
from .mailers import (
    send_email_invite
)

from .schemas import user_input_schema


class UsersAPI(MethodView):
    users_service = inject.attr(UsersService)

    @validate_jsonschema(user_input_schema)
    def post(self):
        try:
            user = self.users_service.persist(**request.json)
            send_email_invite(user)

            return hal.serialize(user)
        except NotUniqueError:
            abort(HTTPStatus.BAD_REQUEST.value)
