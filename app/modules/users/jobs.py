from app.common.rq import rq
from app.common.mail import mail
from app.common.logger import logger


@rq.job
def send_user_invite(to_email):
    logger.info(f"Send email invite to {to_email}")
    mail.send_email(to_email, subject="Welcome to sport stats",
                    text="Welcome to sport stats app")
