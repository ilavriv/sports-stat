from flask.blueprints import Blueprint

from .apis import UsersAPI
from .views import confirm_user_invite

users = Blueprint('users', __name__, template_folder='templates')

users.add_url_rule('/', view_func=UsersAPI.as_view('users_api'))
users.add_url_rule('/p/<confirm_id>', view_func=confirm_user_invite)
