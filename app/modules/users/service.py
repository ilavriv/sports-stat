from mongoengine import DoesNotExist
from passlib.hash import pbkdf2_sha256
from .models import (
    User,
    Confirmation
)


class PasswordParams:
    ROUNDS = 20000
    SALT_SIZE = 16


class UsersService(object):
    def find_by_email(self, email):
        return User.objects.get(email=email)

    def find_by_id(self, user_id):
        return User.objects.get(id=user_id)

    def persist(self, **userData):
        user = User(**userData)
        user.confirmation = Confirmation()
        user.password = pbkdf2_sha256.encrypt(
            userData.get('password'),
            rounds=PasswordParams.ROUNDS, salt_size=PasswordParams.SALT_SIZE)

        user.save()
        return user

    def verify_password(self, user, password):
        return pbkdf2_sha256.verify(password, user.password)

    def confirm_email(self, invite_uid):
        try:
            user = User.objects.get(confirmation__confirm_id=invite_uid)

            if user.confirmation.accepted:
                raise Exception('User invite alredy accepted')

            user.confirmation.accepted = True
            user.save()
        except DoesNotExist:
            pass
