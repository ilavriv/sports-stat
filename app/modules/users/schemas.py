user_input_schema = {
    "$id": "https://example.com/user.schema.json",
    "$schema": "http://json-schema.org/draft-07/schema#",
    'type': 'object',
    'properties': {
        'email': {'type': 'string'},
        'password': {'type': 'string'},
        'first_name': {'type'
                       'string'},
        'last_name': {'type': 'string'}
    },
    'required': [
        'email',
        'password',
        'first_name',
        'last_name'
    ]
}
