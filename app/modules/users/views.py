from flask import render_template
from .service import UsersService


def confirm_user_invite(confirm_id):
    UsersService.confirm_email(confirm_id)

    return render_template('invite_confirmed.html')
