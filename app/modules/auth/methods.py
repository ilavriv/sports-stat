from http import HTTPStatus

from flask import abort
from mongoengine import DoesNotExist

from app.common.logger import logger
from app.modules.users.service import UsersService

from .dto import UserDTO

__all__ = ('authenticate', 'identity',)


def authenticate(email, password):
    try:
        user = UsersService.find_by_email(email=email)
        password_verified = UsersService.verify_password(user, password)

        if not password_verified:
            abort(HTTPStatus.UNAUTHORIZED.value)

        return UserDTO(
            str(user.id),
            user.email,
            user.first_name,
            user.last_name,
            user.created_at,
            user.updated_at
        )
    except DoesNotExist:
        logger.info('User does not exists')
        abort(HTTPStatus.UNAUTHORIZED.value)


def identity(payload):
    user_id = payload.get('identity', None)

    try:
        return UsersService.find_by_id(id=user_id)
    except DoesNotExist:
        abort(HTTPStatus.UNAUTHORIZED.value)
