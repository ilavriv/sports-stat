from dataclasses import make_dataclass

__all__ = ('UserDTO', )

UserDTO = make_dataclass(
    'UserDTO', ('id', 'email', 'first_name', 'last_name', 'created_at',
                'updated_at', ))
