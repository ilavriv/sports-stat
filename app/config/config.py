import os
import datetime


class Config(object):
    """
    Application settings
    """
    DEBUG = os.getenv('DEBUG', default=False)
    TESTING = os.getenv('TESTING', default=False)

    """
    Database settings
    """
    MONGODB_DB = os.getenv('MONGODB_DB', default='flask_app')
    MONGODB_HOST = os.getenv('MONGODB_HOST', default='localhost')
    MONGODB_PORT = os.getenv('MONGODB_PORT', default=27017)

    """
    Flask-JWT settings
    """
    SECRET_KEY = os.getenv(
        'SECRET_KEY', default='7a2658b4-93a1-11e9-bc42-526af7764f64')
    JWT_EXPIRATION_DELTA = os.getenv(
        'JWT_EXPIRATION_DELTA', default=datetime.timedelta(weeks=2))
    JWT_AUTH_USERNAME_KEY = 'email'

    """
    Flask-RQ settings
    """
    RQ_REDIS_URL = 'redis://localhost:6379'

    """
    Flask-SendGrid config
    """
    SENDGRID_API_KEY = (
        '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'
    )
    SENDGRID_DEFAULT_FROM = 'admin@newsapp.com'
