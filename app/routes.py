from .modules.users.urls import users
from .modules.teams.urls import teams
from .modules.sports.urls import sports


def setup_routes(app):
    app.register_blueprint(users, url_prefix='/users')
    app.register_blueprint(teams, url_prefix='/teams')
    app.register_blueprint(sports, url_prefix='/sports')
